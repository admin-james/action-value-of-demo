package action;

import com.opensymphony.xwork2.ActionSupport;
import entity.User;

/**
 * @author James
 */
public class InfoAction extends ActionSupport {
    private User user = new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
